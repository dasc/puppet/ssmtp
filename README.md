# ssmtp

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with ssmtp](#setup)
    * [What ssmtp affects](#what-ssmtp-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with ssmtp](#beginning-with-ssmtp)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

Configure the ssmtp mail client.  This module will install and configure ssmtp
for use with a mail relay.

## Setup

### What ssmtp affects

This module installs the ssmtp package and creates the ssmtp.conf file.

** WARNING **
This module will uninstall postfix from the system, as there have been problems
running postfix and ssmtp on the same host.

### Setup requirements

This modules requires that you have an unauthenticated mail relay
already configured to accept mail from your hosts.

### Beginning with ssmtp

Install ssmtp and point it at your local mail relay:

```
    class {'::ssmtp':
	mailhub => '10.13.5.16',
    }
```

## Usage

Install ssmtp and point it at your local mail relay:

```
    class {'::ssmtp':
	mailhub => '10.13.5.16',
    }
```

## Reference

#### Parameters

The following parameters are available in the `ssmtp` class.

##### `mailhub`

Data type: `String`

The hostname or IP address of the local unauthenticated mail relay

##### `hostname`

Data type: `Optional[String]`

The full hostname of "this" system

Default value: $facts[fqdn]

##### `rewrite_domain`

Data type: `Optional[String]`

The domain that mail is rewritten to appear to come from

Default value: `undef`

##### `root_user`

Data type: `Optional[String]`

The person who gets all mail for userids < 1000

Default value: 'postmaster'

##### `allow_from_override`

Data type: `Optional[Boolean]`

Control whether From: line modification is allowed by the submitting user

Default value: `true`


## Limitations

This module has only been tested on RHEL 7 based systems.  Compatibility with
other OSes is not guaranteed, but patches are welcome.

## Development

Since your module is awesome, other users will want to play with it. Let them
know what the ground rules for contributing are.

## Release Notes/Contributors/Etc. **Optional**

If you aren't using changelog, put your release notes here (though you should
consider using changelog). You can also add any additional sections you feel
are necessary or important to include here. Please use the `## ` header.
