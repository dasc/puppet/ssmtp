# The ssmtp class
#
# @summary installs and configures the ssmtp mail transport agent
#
# @example Basic usage
#   class { '::ssmtp':
#       mailhub => '10.0.0.1',
#   }
#
# @param mailhub
#   The hostname or IP address of the local unauthenticated mail relay
#
# @param hostname
#   The full hostname of "this" system
#
# @param rewrite_domain
#   The domain that mail is rewritten to appear to come from
#
# @param root_user
#   The person who gets all mail for userids < 1000
#
# @param allow_from_override
#   Control whether From: line modification is allowed by the submitting user
#
class ssmtp (
    String            $mailhub,
    Optional[String]  $hostname            = $facts[fqdn],
    Optional[String]  $rewrite_domain      = undef,
    Optional[String]  $root_user           = 'postmaster',
    Optional[Boolean] $allow_from_override = true,
) {
    package { 'ssmtp': ensure => installed, }

    package { 'pcp-pmda-postfix': ensure => absent, } ->
    package { 'postfix-perl-scripts': ensure => absent, } ->
    package { 'postfix': ensure => absent, }

    package { 'exim4': ensure => absent, }

    file { '/etc/ssmtp/ssmtp.conf':
        ensure  => file,
        owner   => 'root',
        group   => 'mail',
        mode    => '0644',
        require => Package['ssmtp'],
        content => epp('ssmtp/etc/ssmtp/ssmtp.conf.epp', {
            'mailhub'             => $mailhub,
            'rewrite_domain'      => $rewrite_domain,
            'root_user'           => $root_user,
            'allow_from_override' => $allow_from_override,
        })
    }
}
